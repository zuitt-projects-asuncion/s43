// alert("hello")

let posts = [];
let count = 1;

// Add post

document.querySelector("#form-add-post").addEventListener("submit", (e) => {

	e.preventDefault();

	posts.push({

		id: count,
		title: document.querySelector("#txt-title").value,
		body: document.querySelector("#txt-body").value
	})

	count++

	showPosts(posts);
	alert("Post successfully added");
});

const showPosts = (posts) => {

	let postEntries = '';

	posts.forEach((post) => {

		console.log(post)

		postEntries += `
		<div id="post-${post.id}">

			<h3 id="post-title-${post.id}">${post.title}</h3>

			<p id="post-body-${post.id}">${post.body}</p>

			<button onclick="editPost('${post.id}')">Edit</button>

			<button onclick="deletePost('${post.id}')">Delete</button>
		</div>

		`;
	});

	document.querySelector("#div-post-entries").innerHTML = postEntries;
};

// Edit post function

const editPost = (id) => {

	let title = document.querySelector(`#post-title-${id}`).innerHTML;

	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector("#txt-edit-id").value = id;

	document.querySelector("#txt-edit-title").value = title;

	document.querySelector("#txt-edit-body").value = body;
};

// Update Post

document.querySelector("#form-edit-post").addEventListener("submit", (e) => {

	e.preventDefault();

	for(let i = 0; i < posts.length; i++){

		if(posts[i].id.toString() === document.querySelector("#txt-edit-id").value){

			posts[i].title = document.querySelector("#txt-edit-title").value;

			posts[i].body = document.querySelector("#txt-edit-body").value;

			showPosts(posts)
			alert("Post successfully updated");

			break;
		}
	}
});

// Delete post function

const deletePost = (id) => {

	let title = document.querySelector(`#post-title-${id}`).innerHTML;

	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector("#txt-delete-id").value = id;

	document.querySelector("#txt-delete-title").value = title;

	document.querySelector("#txt-delete-body").value = body;
};


// Delete Post

document.querySelector("#form-delete-post").addEventListener("submit", (e) => {

	e.preventDefault();

	for(let i = 0; i < posts.length; i++){

		if(posts[i].id.toString() === document.querySelector("#txt-delete-id").value){

			/*posts[i].title =*/ document.querySelector("#txt-title").value = '';

			/*posts[i].body = */document.querySelector("#txt-body").value = '';

			showPosts(posts)
			alert("Post deleted");

			break;
		}
	}
});




// Activity Instructions

/*
	 >> Using the code so far, write the necessary code to delete a post.
	 
	 >> Function name should be deletePost

*/
